//required by vJoy
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <basetyps.h>
#include <cfgmgr32.h>
#include <Setupapi.h>
#include <strsafe.h>
#include <Newdev.h>
#include <INITGUID.H>

//vJoy headers
#include "public.h"
#include "vjoyinterface.h"

//ros and package headers
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ets_msg/steeringCommand.h"


// Default device ID (Used when ID not specified)
#define DEV_ID		1
#define AXIS_MAX	32000

JOYSTICK_POSITION_V2 iReport; // The structure that holds the full position data

void callback(const ets_msg::steeringCommand::ConstPtr& msg) {
	float target = msg->axis_position;
	USHORT targetValue= (USHORT)(target * (float)AXIS_MAX);

	ROS_DEBUG("target: %f, value: %hu", target, targetValue);


	UINT DevID = DEV_ID;
	PVOID pPositionMessage;

	// Set destenition vJoy device
	iReport.bDevice = (BYTE)DevID;
	
	//set axis
	iReport.wAxisX = targetValue;
	iReport.wAxisY = AXIS_MAX/2;
	iReport.wAxisZ = AXIS_MAX/2;

	pPositionMessage = (PVOID)(&iReport);

	AcquireVJD(DevID);
	if (!UpdateVJD(DevID, pPositionMessage))
	{
		printf("Feeding vJoy device number %d failed - try to enable device\n", DevID);
	}

}

int main(int argc, char** argv){

	ros::init(argc, argv, "steering");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("/command/steering", 1000, callback);
	ros::spin();
}