﻿import rospy,mss,cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
import numpy as np

class Recorder():

    def __init__(self):
        self.pub=rospy.Publisher('/image/bgra8',Image,queue_size=10)
        rospy.init_node('screencapture',anonymous=True)
        self.bridge=CvBridge()


    def capture(self,rate=30):
        r=rospy.Rate(rate)
        with mss.mss() as sct:
            while not rospy.is_shutdown():

                monitor_number = 2 #TODO get from Parameter
                mon = sct.monitors[monitor_number]

                img = np.array(sct.grab(mon))

                self.pub.publish(self.bridge.cv2_to_imgmsg(img,"bgra8"))
                r.sleep()








if __name__=='__main__':
    
    rec=Recorder()
    rec.capture(30)


