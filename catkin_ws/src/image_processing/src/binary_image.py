#!/usr/bin/env python
# coding=utf-8

import rospy,cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
import numpy as np

class BinaryImageNode():
    
    def callback(self,data):
        print('callback')
        image=self.bridge.imgmsg_to_cv2(data,"bgra8")#umwandeln in cv
        _,binIm=cv2.threshold(image,230,255,cv.THRESH_BINARY)
        #schwarze Dreiecke drüberzeichnen
        #cv2.rectangle(binIm,(0,0),(639,105),(0,0,0),-1)
        #cv2.rectangle(binIm,(0,245),(639,479),(0,0,0),-1)
        self.bin_pub.publish(self.bridge.cv2_to_imgmsg(binIm,"bgra8"))#bild wieder publishen

    def __init__(self):
        print('init') 
        self.bridge=CvBridge()
        self.bin_pub=rospy.Publisher('/image/mono8',Image,queue_size=10)#publisht das binary_image
        rospy.init_node('BinaryImage', anonymous=True)
        #rospy.Subscriber('/sensors/camera/infra1/camera_info', CameraInfo, self.cInfo)
        rospy.Subscriber('/image/bgra8', Image, self.callback)
        rospy.spin()


if __name__ == "__main__":
    BinaryImageNode()
